#！/bin/bash
LISTEN(){
    ss -an|grep '^tcp'|grep 'LISTEN'|wc -l
}

SYN_RECV(){
    ss -an|grep '^tcp'|grep 'SYN[_-]RECV'|wc -l
}

ESTABLISHED(){
    ss -an|grep '^tcp'|grep 'ESAB'|wc -l
}

TIME_WAIT(){
    ss -an|grep '^tcp'|grep 'TIME[_-]WAIT'|wc -l
}
$1

在zabbix的zabbix_agentd.conf
把UnsafeUserParameters=0改为1
在最后添加UserParameters=key名+命令
例如：
[root@localhost scripts]# tail /usr/local/etc/zabbix_agentd.conf
UserParameter=check_p[*],/bin/bash /scripts/check_process.sh $1


内存信息收集（配合zabbix）
/proc/meminfo
#!/bin/bash
MemTotal() {
   awk ‘/^MemTotal/{print $2}’ /proc/meminfo 
}
MomFree() {
  awk ‘/^MomFree/{print $2}’ /proc/meminfo
}
//内存的脏页情况
Dirty() {
  awk ‘/^Dirty/{print $2}’ /proc/meminfo
}
Buffers() {
  awk ‘/^Buffers/{print $2}’ /proc/meminfo
}
$1
I/O状态采集
系统的IO 利用率：　(利用iostat)
#!/bin/bash
io=`iostat -x -k |awk '{print $12}'|sort -rn -t ' ' -k 2 | head -2|sed -n 2p 2>/dev/null`
echo $io

echo $ioMySQL状态信息采集（配合zabbix）
Mysql状态：mysqladmin status
扩展状态信息：mysqladmin extended-status |less
#！/bin/bash
#mysql for zabbix
//运行时间
Uptime() {
    mysqladmin status |awk ‘{print $2}’
}
Slow_queries(){
    mysqladmin status |awk ‘{print $9}’
}
Com_delete(){
    mysqladmin extened-status|awk ‘/\<Com_delete\>/{pent $4}’
}
Com_update(){
    mysqladmin extened-status|awk ‘/\<Com_update\>/{pent $4}’
}
Com_select(){
    mysqladmin extened-status|awk ‘/\<Com_select\>/{pent $4}’
}
Com_insert(){
    mysqladmin extened-status|awk ‘/\<Com_insert\>/{pent $4}’
}
Com_commit(){
    mysqladmin extened-status|awk ‘/\<Com_commit\>/{pent $4}’
}
Com_rollback(){
    mysqladmin extened-status|awk ‘/\<Com_rollback\>/{pent $4}’
}
$1
